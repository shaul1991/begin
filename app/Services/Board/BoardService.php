<?php

declare(strict_types=1);


namespace App\Services\Board;


use App\Models\Board;
use App\Models\User;
use App\Services\Board\Dto\BoardDto;
use App\Services\Board\Exceptions\NotMatchedBoardWriterException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BoardService
{
    private Board $board;

    public function __construct(Board $board)
    {
        $this->board = $board;
    }

    public function all(int $userId = null): array|Collection
    {
        $boards = $this->board->latest();

        if (!empty($userId)) {
            $boards = $boards->where('user_id', '=', $userId);
        }

        return $boards->get();
    }

    public function getPublicBoards(int $userId = null): array|Collection
    {
        $boards = $this->board->latest()->where('is_public', true);

        if (!empty($userId)) {
            $boards = $boards->where('user_id', '=', $userId);
        }

        return $boards->get();
    }

    public function getPrivateBoards(int $userId = null): array|Collection
    {
        $boards = $this->board->latest()->where('is_public', false);

        if (!empty($userId)) {
            $boards = $boards->where('user_id', '=', $userId);
        }

        return $boards->get();
    }

    public function save(User $user, BoardDto $dto): Model|bool
    {
        return $user->boards()->save($this->board->fill($dto->toArray()));
    }

    /**
     * @param  User  $user
     * @param  Board  $board
     * @param  BoardDto  $dto
     * @return Board
     * @throws NotMatchedBoardWriterException
     */
    public function update(User $user, Board $board, BoardDto $dto): Board
    {
        if ($user->id !== $board->user_id) {
            throw new NotMatchedBoardWriterException();
        }

        $board->update($dto->toArray());

        return $board;
    }

    /**
     * @param  User  $user
     * @param  Board  $board
     * @return bool|null
     * @throws NotMatchedBoardWriterException
     */
    public function destroy(User $user, Board $board): ?bool
    {
        if ($user->id !== $board->user_id) {
            throw new NotMatchedBoardWriterException();
        }

        return $board->delete();
    }
}
