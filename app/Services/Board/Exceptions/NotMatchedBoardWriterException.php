<?php

declare(strict_types=1);


namespace App\Services\Board\Exceptions;


use App\Exceptions\BadRequestException;

class NotMatchedBoardWriterException extends BadRequestException
{
    protected $message = "게시물의 작성자가 아닙니다.";
}
