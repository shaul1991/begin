<?php

declare(strict_types=1);


namespace App\Services\Board\Dto;


use App\Dto\Dto;

class BoardDto extends Dto
{
    public function __construct(
        protected string $title,
        protected string $contents,
        protected bool $is_public
    ) {
    }
}
