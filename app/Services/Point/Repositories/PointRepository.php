<?php

declare(strict_types=1);


namespace App\Services\Point\Repositories;


use App\Models\PointHistory;
use Illuminate\Database\Eloquent\Model;

class PointRepository
{
    private PointHistory $pointHistory;

    public function __construct(PointHistory $pointHistory)
    {
        $this->pointHistory = $pointHistory;
    }

    public function savePointHistory(array $args): Model|PointHistory
    {
        return $this->pointHistory->firstOrCreate($args);
    }
}
