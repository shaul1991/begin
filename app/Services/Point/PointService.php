<?php

declare(strict_types=1);


namespace App\Services\Point;


use App\Models\PointHistory;
use App\Models\User;
use App\Services\Point\Dto\SavePointDto;
use App\Services\Point\Repositories\PointRepository;
use Illuminate\Database\Eloquent\Model;

class PointService
{
    private PointRepository $pointRepository;

    public function __construct(PointRepository $pointRepository)
    {
        $this->pointRepository = $pointRepository;
    }

    public function savePoint(SavePointDto $dto): Model|PointHistory
    {
        return $this->pointRepository->savePointHistory($dto->toArray());
    }

    public function syncPoint(User $user): User
    {
        $user->point = $user->point_histories->sum('point');
        $user->timestamps = false;
        $user->update();

        return $user;
    }
}
