<?php

declare(strict_types=1);


namespace App\Services\Point\Dto;


use App\Dto\Dto;

class SavePointDto extends Dto
{
    public function __construct(
        protected int $user_id,
        protected string $cause,
        protected int $point,
    ) {
    }
}
