<?php

declare(strict_types=1);


namespace App\Services\User\Exceptions;


use Exception;
use Symfony\Component\HttpFoundation\Response;

class CannotGrantSameOrHigherAuthorityThanYourselfException extends Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;

    protected $message = "자신과 같거나 더 높은 권한을 부여 할 수 없습니다.";
}
