<?php

declare(strict_types=1);


namespace App\Services\User\Exceptions;


use Exception;
use Symfony\Component\HttpFoundation\Response;

class CannotChangeLevelYourselfException extends Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;

    protected $message = "자기 자신의 등급을 조정할 수 없습니다.";
}
