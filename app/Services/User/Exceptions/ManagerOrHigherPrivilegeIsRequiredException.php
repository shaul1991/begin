<?php

declare(strict_types=1);


namespace App\Services\User\Exceptions;


use App\Models\User;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ManagerOrHigherPrivilegeIsRequiredException extends Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->message = "[".User::USER_LEVEL_DESCRIPTION[User::USER_LEVEL_MANAGER]."] 레벨 이상의 유저 권한입니다.";
    }
}
