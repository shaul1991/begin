<?php

declare(strict_types=1);


namespace App\Services\User;


use App\Models\User;
use App\Services\User\Exceptions\CannotChangeLevelYourselfException;
use App\Services\User\Exceptions\CannotGrantSameOrHigherAuthorityThanYourselfException;
use App\Services\User\Exceptions\ManagerOrHigherPrivilegeIsRequiredException;

class UpdateUserLevelService
{
    /**
     * @param  User  $admin
     * @param  User  $user
     * @param  int  $level
     * @return User
     * @throws ManagerOrHigherPrivilegeIsRequiredException
     * @throws CannotChangeLevelYourselfException
     * @throws CannotGrantSameOrHigherAuthorityThanYourselfException
     */
    public function update(User $admin, User $user, int $level): User
    {
        if (!$admin->isManagerOrHigher()) {
            throw new ManagerOrHigherPrivilegeIsRequiredException();
        }

        if ($admin->id === $user->id) {
            throw new CannotChangeLevelYourselfException();
        }

        if ($admin->level <= $level) {
            throw new CannotGrantSameOrHigherAuthorityThanYourselfException();
        }

        $user->update(['level' => $level]);

        return $user;
    }
}
