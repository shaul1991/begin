<?php

declare(strict_types=1);


namespace App\Services\User\Dto;


use App\Dto\Dto;

class RegisterDto extends Dto
{
    public function __construct(
        protected string $name,
        protected string $email,
        protected string $password
    ) {
    }
}
