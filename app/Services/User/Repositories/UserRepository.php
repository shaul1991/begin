<?php

declare(strict_types=1);


namespace App\Services\User\Repositories;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(array $args): User|Model
    {
        return $this->user->create($args);
    }
}
