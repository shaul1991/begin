<?php

declare(strict_types=1);


namespace App\Services\User;


use App\Models\User;

class UpdateLastLoginService
{
    public function update(User $user, string|null $ip): User
    {
        $user->last_login_at = now()->toDateString();
        $user->last_login_ip = $ip;
        $user->timestamps = false;
        $user->update();

        return $user;
    }
}
