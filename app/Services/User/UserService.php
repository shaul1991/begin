<?php

declare(strict_types=1);


namespace App\Services\User;


use App\Models\User;
use App\Services\User\Dto\RegisterDto;
use App\Services\User\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;

class UserService
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(RegisterDto $dto): User|Model
    {
        return $this->userRepository->create($dto->toArray());
    }
}
