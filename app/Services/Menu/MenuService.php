<?php

declare(strict_types=1);


namespace App\Services\Menu;


use App\Models\Menu;
use App\Services\Menu\Dto\MainMenuDto;
use App\Services\Menu\Dto\SubMenuDto;
use App\Services\Menu\Exceptions\ExistsSubMenuException;
use App\Services\Menu\Repositories\MenuRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class MenuService
{
    private MenuRepository $menuRepository;

    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function get(?int $limit = null): Collection
    {
        return $this->menuRepository->get($limit)->each(function (Menu $menu) {
            return $menu->where('is_exposed', true);
        });
    }

    public function mainMenuCreate(MainMenuDto $dto): Model|Menu
    {
        return $this->menuRepository->mainMenuCreate($dto->toArray());
    }

    public function mainMenuUpdate(Menu $mainMenu, MainMenuDto $dto): bool
    {
        return $this->menuRepository->mainMenuUpdate($mainMenu, $dto->toArray());
    }

    public function mainMenuDestroy(Menu $menu): int
    {
        if ($menu->sub_menus->isNotEmpty()) {
            throw new ExistsSubMenuException();
        }

        return $this->menuRepository->mainMenuDestroy($menu->id);
    }

    public function subMenuCreate(Menu $menu, SubMenuDto $dto): Model|bool
    {
        return $this->menuRepository->subMenuCreate($menu, $dto->toArray());
    }

    public function subMenuUpdate(Menu $subMenu, SubMenuDto $dto): bool
    {
        return $this->menuRepository->subMenuUpdate($subMenu, $dto->toArray());
    }
}
