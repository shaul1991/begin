<?php

declare(strict_types=1);


namespace App\Services\Menu\Exceptions;


use App\Exceptions\BadRequestException;

class ExistsSubMenuException extends BadRequestException
{
    protected $code = 400;

    protected $message = '하위 메뉴가 존재하여 삭제할 수 없습니다.';
}
