<?php

declare(strict_types=1);


namespace App\Services\Menu\Dto;


use App\Dto\Dto;
use App\Models\Menu;

class SubMenuDto extends Dto
{
    protected int $depth = Menu::DEPTH_SUB;

    public function __construct(
        protected string $name,
        protected bool $is_exposed = false,
        protected string $url = ''
    ) {
    }
}
