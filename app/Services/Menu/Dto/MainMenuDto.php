<?php

declare(strict_types=1);


namespace App\Services\Menu\Dto;


use App\Dto\Dto;
use App\Models\Menu;

class MainMenuDto extends Dto
{
    protected int $depth = Menu::DEPTH_MAIN;

    public function __construct(
        protected string $name,
        protected bool $is_exposed
    ) {
    }
}
