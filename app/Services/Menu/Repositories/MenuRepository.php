<?php

declare(strict_types=1);


namespace App\Services\Menu\Repositories;


use App\Models\Menu;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class MenuRepository
{
    private Menu $menu;

    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }

    public function get(?int $limit): Collection
    {
        $query = $this->menu->mainMenu()->isExpose();

        if(!empty($limit)) {
            $query->limit($limit);
        }

        return $query->get();
    }

    public function mainMenuCreate(array $args): Model|Menu
    {
        return $this->menu->firstOrCreate($args);
    }

    public function mainMenuDestroy(int $id): int
    {
        return $this->menu->destroy($id);
    }

    public function mainMenuUpdate(Menu $mainMenu, array $args): bool
    {
        return $mainMenu->update($args);
    }

    public function subMenuCreate(Menu $menu, array $args): Model|bool
    {
        return $menu->sub_menus()->save($this->menu->fill($args));
    }

    public function subMenuUpdate(Menu $subMenu, array $args): bool
    {
        return $subMenu->update($args);
    }
}
