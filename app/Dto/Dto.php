<?php

declare(strict_types=1);


namespace App\Dto;


use Illuminate\Contracts\Support\Arrayable;

abstract class Dto implements Arrayable
{
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
