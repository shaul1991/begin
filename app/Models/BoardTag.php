<?php

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Carbon;

/**
 * App\Models\BoardTag
 *
 * @property int $board_id
 * @property int $tag_id
 * @property Carbon $created_at
 * @method static Builder|BoardTag newModelQuery()
 * @method static Builder|BoardTag newQuery()
 * @method static Builder|BoardTag query()
 * @method static Builder|BoardTag whereBoardId($value)
 * @method static Builder|BoardTag whereCreatedAt($value)
 * @method static Builder|BoardTag whereTagId($value)
 * @mixin Eloquent
 */
class BoardTag extends Pivot
{
    public const UPDATED_AT = false;
}
