<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $point 포인트
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $level 유저 등급
 * @property string|null $last_login_at 최근 접속 일시
 * @property string|null $last_login_ip 최근 접속 IP
 * @property Carbon|null $deleted_at
 * @property-read Collection|\App\Models\Board[] $boards
 * @property-read int|null $boards_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|\App\Models\PointHistory[] $point_histories
 * @property-read int|null $point_histories_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastLoginAt($value)
 * @method static Builder|User whereLastLoginIp($value)
 * @method static Builder|User whereLevel($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePoint($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use SoftDeletes;

    public const USER_LEVEL_DESCRIPTION = [
        self::USER_LEVEL_PERMANENT_STOP => '영구정지',
        self::USER_LEVEL_PAUSE => '일시정지',
        self::USER_LEVEL_DORMANT => '휴면',
        self::USER_LEVEL_WAITING => '대기',
        self::USER_LEVEL_GENERAL => '일반',
        self::USER_LEVEL_MANAGER => '매니저',
        self::USER_LEVEL_ADMIN => '관리자',
        self::USER_LEVEL_SYSTEM_ADMIN => '시스템 관리자',
        self::USER_LEVEL_SYSTEM => '시스템',
    ];

    public const USER_LEVEL_PERMANENT_STOP = -99;
    public const USER_LEVEL_PAUSE = -98;
    public const USER_LEVEL_DORMANT = -97;
    public const USER_LEVEL_WAITING = 0;
    public const USER_LEVEL_GENERAL = 1;
    public const USER_LEVEL_MANAGER = 97;
    public const USER_LEVEL_ADMIN = 98;
    public const USER_LEVEL_SYSTEM_ADMIN = 99;
    public const USER_LEVEL_SYSTEM = 100;

    protected $fillable = [
        'name',
        'email',
        'password',
        'level',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $attributes = [
        'point' => 0,
        'level' => self::USER_LEVEL_GENERAL,
    ];

    public function point_histories(): HasMany
    {
        return $this->hasMany(PointHistory::class);
    }

    public function isManagerOrHigher(): bool
    {
        return $this->level >= self::USER_LEVEL_MANAGER;
    }

    public function boards(): HasMany
    {
        return $this->hasMany(Board::class);
    }
}
