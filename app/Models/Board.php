<?php

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\Board
 *
 * @property int $id
 * @property int $user_id
 * @property string $title 제목
 * @property string|null $contents 내용
 * @property int $views 조회수
 * @property int $is_public 공개글 여부
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Collection|\App\Models\Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read \App\Models\User $user
 * @method static \Database\Factories\BoardFactory factory(...$parameters)
 * @method static Builder|Board newModelQuery()
 * @method static Builder|Board newQuery()
 * @method static \Illuminate\Database\Query\Builder|Board onlyTrashed()
 * @method static Builder|Board query()
 * @method static Builder|Board whereContents($value)
 * @method static Builder|Board whereCreatedAt($value)
 * @method static Builder|Board whereDeletedAt($value)
 * @method static Builder|Board whereId($value)
 * @method static Builder|Board whereIsPublic($value)
 * @method static Builder|Board whereTitle($value)
 * @method static Builder|Board whereUpdatedAt($value)
 * @method static Builder|Board whereUserId($value)
 * @method static Builder|Board whereViews($value)
 * @method static \Illuminate\Database\Query\Builder|Board withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Board withoutTrashed()
 * @mixin Eloquent
 */
class Board extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'contents',
        'views',
        'is_public',
    ];

    protected $attributes = [
        'views' => 0,
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }
}
