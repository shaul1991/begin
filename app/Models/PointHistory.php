<?php

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\PointHistory
 *
 * @property int $id
 * @property int $user_id
 * @property string $cause 사유
 * @property int $point 포인트
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read \App\Models\User $user
 * @method static \Database\Factories\PointHistoryFactory factory(...$parameters)
 * @method static Builder|PointHistory newModelQuery()
 * @method static Builder|PointHistory newQuery()
 * @method static \Illuminate\Database\Query\Builder|PointHistory onlyTrashed()
 * @method static Builder|PointHistory query()
 * @method static Builder|PointHistory whereCause($value)
 * @method static Builder|PointHistory whereCreatedAt($value)
 * @method static Builder|PointHistory whereDeletedAt($value)
 * @method static Builder|PointHistory whereId($value)
 * @method static Builder|PointHistory wherePoint($value)
 * @method static Builder|PointHistory whereUpdatedAt($value)
 * @method static Builder|PointHistory whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|PointHistory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PointHistory withoutTrashed()
 * @mixin Eloquent
 */
class PointHistory extends Model
{
    use HasFactory;
    use SoftDeletes;

    public const HAS_DEFAULT_POINT = 0;

    public const CAUSE_DESCRIPTION = [
        self::CAUSE_SIGN_UP => '회원가입',
    ];

    public const CAUSE_POINT = [
        self::CAUSE_SIGN_UP => 500,
    ];

    public const CAUSE_SIGN_UP = 1;

    protected $fillable = [
        'user_id',
        'cause',
        'point',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
