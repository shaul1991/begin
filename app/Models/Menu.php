<?php

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\Menu
 *
 * @property int $id
 * @property int $depth 메인메뉴/서브메뉴
 * @property string $name 메뉴 이름
 * @property int $is_exposed 노출 여부
 * @property int|null $parent_id 상위메뉴 ID
 * @property string|null $url url
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Collection|Menu[] $sub_menus
 * @property-read int|null $sub_menus_count
 * @method static \Database\Factories\MenuFactory factory(...$parameters)
 * @method static Builder|Menu isExpose()
 * @method static Builder|Menu mainMenu()
 * @method static Builder|Menu newModelQuery()
 * @method static Builder|Menu newQuery()
 * @method static \Illuminate\Database\Query\Builder|Menu onlyTrashed()
 * @method static Builder|Menu query()
 * @method static Builder|Menu whereCreatedAt($value)
 * @method static Builder|Menu whereDeletedAt($value)
 * @method static Builder|Menu whereDepth($value)
 * @method static Builder|Menu whereId($value)
 * @method static Builder|Menu whereIsExposed($value)
 * @method static Builder|Menu whereName($value)
 * @method static Builder|Menu whereParentId($value)
 * @method static Builder|Menu whereUpdatedAt($value)
 * @method static Builder|Menu whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|Menu withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Menu withoutTrashed()
 * @mixin Eloquent
 */
class Menu extends Model
{
    use HasFactory;
    use SoftDeletes;

    public const DEPTH_DESCRIPTION = [
        self::DEPTH_MAIN => '메인메뉴',
        self::DEPTH_SUB => '서브메뉴',
    ];

    public const DEPTH_MAIN = 1;
    public const DEPTH_SUB = 2;

    protected $fillable = [
        'name',
        'depth',
        'is_exposed',
        'parent_id',
        'url',
    ];

    protected $with = ['sub_menus'];

    public function sub_menus(): HasMany
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id');
    }

    public function scopeMainMenu(Builder $builder): Builder
    {
        return $builder->where('depth', '=', self::DEPTH_MAIN);
    }

    public function scopeIsExpose(Builder $builder): Builder
    {
        return $builder->where('is_exposed', '=', true);
    }
}
