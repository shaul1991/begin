<?php

declare(strict_types=1);


namespace App\Exceptions;


use Exception;
use Symfony\Component\HttpFoundation\Response;

abstract class BadRequestException extends Exception
{
    public const HTTP_STATUS_CODE = Response::HTTP_BAD_REQUEST;
}
