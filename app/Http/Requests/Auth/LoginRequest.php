<?php

declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class LoginRequest
 * @package App\Http\Requests\Auth
 * @property-read string $email
 * @property-read string $password
 */
class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', 'max:255', Rule::exists(User::class, 'email')],
            'password' => ['required', 'min:8'],
        ];
    }

    public function messages(): array
    {
        return [
            'email.email' => ':attribute의 형식이 아닙니다.',
            'email.max' => ':attribute은 :max자를 넘어갈 수 없습니다.',
            'email.exists' => '가입되지 않은 :attribute 입니다.',
            'password.min' => ':attribute는 최소 :min 자를 입력해주세요.',
        ];
    }

    public function attributes(): array
    {
        return [
            'email' => '이메일',
            'password' => '비밀번호',
        ];
    }
}
