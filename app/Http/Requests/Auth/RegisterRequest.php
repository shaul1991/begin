<?php

declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Auth
 * @property-read string $name
 * @property-read string $email
 * @property-read string $password
 */
class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique(User::class, 'email')],
            'password' => ['required', 'min:8', 'confirmed'],
        ];
    }

    public function messages(): array
    {
        return [
            'name.max' => ':attribute은 :max자를 넘어갈 수 없습니다.',
            'email.email' => ':attribute의 형식이 아닙니다.',
            'email.max' => ':attribute은 :max자를 넘어갈 수 없습니다.',
            'email.unique' => '이미 가입된 :attribute 입니다.',
            'password.min' => ':attribute는 최소 :min 자를 입력해주세요.',
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => '이름',
            'email' => '이메일',
            'password' => '비밀번호',
        ];
    }
}
