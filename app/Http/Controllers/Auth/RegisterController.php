<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\PointHistory;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\Point\Dto\SavePointDto;
use App\Services\Point\PointService;
use App\Services\User\Dto\RegisterDto;
use App\Services\User\UpdateLastLoginService;
use App\Services\User\UserService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected string $redirectTo = RouteServiceProvider::HOME;
    private UserService $userService;
    private PointService $pointService;
    private UpdateLastLoginService $updateLastLoginService;

    public function __construct(
        UserService $userService,
        PointService $pointService,
        UpdateLastLoginService $updateLastLoginService
    ) {
        $this->middleware('guest');
        $this->userService = $userService;
        $this->pointService = $pointService;
        $this->updateLastLoginService = $updateLastLoginService;
    }

    public function register(RegisterRequest $request): Redirector|Application|RedirectResponse
    {
        $dto = new RegisterDto(
            (string) $request->name,
            (string) $request->email,
            (string) Hash::make($request->password)
        );

        /** @var User $user */
        $user = $this->userService->create($dto);

        event(new Registered($user));

        $user->refresh();

        $this->guard()->login($user);

        $this->addPointToSignUp($user);
        $this->updateLastLoginService->update($user, $request->getClientIp());

        return redirect($this->redirectPath());
    }

    protected function addPointToSignUp(User $user): void
    {
        $dto = new SavePointDto(
            $user->id,
            PointHistory::CAUSE_DESCRIPTION[PointHistory::CAUSE_SIGN_UP],
            PointHistory::CAUSE_POINT[PointHistory::CAUSE_SIGN_UP]
        );

        $this->pointService->savePoint($dto);
        $this->pointService->syncPoint($user);
    }
}
