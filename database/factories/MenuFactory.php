<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    protected $model = Menu::class;

    public function definition(): array
    {
        return [
            'depth' => $this->faker->randomKey(Menu::DEPTH_DESCRIPTION),
            'name' => $this->faker->name,
            'is_exposed' => $this->faker->boolean,
            'url' => $this->faker->url,
            'parent_id' => $this->faker->randomNumber(1),
        ];
    }

    public function mainMenu(): MenuFactory
    {
        return $this->state([
            'url' => null,
            'depth' => Menu::DEPTH_MAIN,
            'parent_id' => null,
        ]);
    }

    public function subMenu(Menu $mainMenu): MenuFactory
    {
        return $this->state([
            'url' => config('app.url'),
            'depth' => Menu::DEPTH_SUB,
            'parent_id' => $mainMenu->id,
        ]);
    }
}
