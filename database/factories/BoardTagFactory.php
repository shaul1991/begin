<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Board;
use App\Models\BoardTag;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

class BoardTagFactory extends Factory
{
    protected $model = BoardTag::class;

    public function definition(): array
    {
        return [
            'board_id' => Board::factory(),
            'tag_id' => Tag::factory(),
        ];
    }
}
