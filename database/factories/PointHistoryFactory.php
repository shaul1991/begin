<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\PointHistory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointHistoryFactory extends Factory
{
    protected $model = PointHistory::class;

    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'cause' => $this->faker->text,
            'point' => (int) $this->faker->numberBetween(-500, 500),
        ];
    }
}
