<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Board;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BoardFactory extends Factory
{
    protected $model = Board::class;

    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'title' => $this->faker->title,
            'contents' => $this->faker->sentences(random_int(1, 5), true),
            'views' => $this->faker->randomNumber(),
            'is_public' => $this->faker->boolean,
        ];
    }
}
