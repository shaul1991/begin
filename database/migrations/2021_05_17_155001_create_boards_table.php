<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardsTable extends Migration
{
    public function up()
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index();
            $table->string('title')->comment('제목');
            $table->text('contents')->nullable()->comment('내용');
            $table->unsignedBigInteger('views')->default(0)->comment('조회수');
            $table->boolean('is_public')->default(true)->comment('공개글 여부');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('boards');
    }
}
