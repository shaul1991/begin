<?php

declare(strict_types=1);

use App\Models\Board;
use App\Models\Tag;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardTagTable extends Migration
{
    public function up(): void
    {
        Schema::create('board_tag', function (Blueprint $table) {
            $table->foreignIdFor(Board::class)->index();
            $table->foreignIdFor(Tag::class)->index();
            $table->dateTime('created_at')->useCurrent();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('board_tag');
    }
}
