<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FeatureUserPoint extends Migration
{
    public function up()
    {
        Schema::create('point_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('cause')->comment('사유');
            $table->bigInteger('point')->comment('포인트');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('point')->after('email')->default(0)->comment('포인트');
        });
    }

    public function down()
    {
        Schema::dropIfExists('point_histories');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('point');
        });
    }
}
