<?php

declare(strict_types=1);

use App\Models\Menu;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('depth')->default(Menu::DEPTH_MAIN)->comment('메인메뉴/서브메뉴');
            $table->string('name')->comment('메뉴 이름');
            $table->boolean('is_exposed')->default(false)->comment('노출 여부');
            $table->foreignId('parent_id')->nullable()->comment('상위메뉴 ID');
            $table->text('url')->nullable()->comment('url');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
