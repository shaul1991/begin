<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\PointHistory;
use Illuminate\Database\Seeder;

class PointHistorySeeder extends Seeder
{
    public function run(): void
    {
        if (app()->environment() !== 'production') {
            PointHistory::factory()
                ->count(50)
                ->create();
        }
    }
}
