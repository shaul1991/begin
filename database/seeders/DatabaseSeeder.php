<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(MenuSeeder::class);
        $this->call(PointHistorySeeder::class);
        $this->call(TagSeeder::class);
        $this->call(BoardSeeder::class);
        $this->call(BoardTagSeeder::class);
    }
}
