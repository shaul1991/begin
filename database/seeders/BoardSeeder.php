<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Board;
use Illuminate\Database\Seeder;

class BoardSeeder extends Seeder
{
    public function run(): void
    {
        if (app()->environment() !== 'production') {
            Board::factory()
                ->count(50)
                ->create();
        }
    }
}
