<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    public function run(): void
    {
        if (app()->environment() !== 'production') {
            $menuFactory = Menu::factory();

            $menuFactory->mainMenu()
                ->count(random_int(3, 5))
                ->create()
                ->each(function (Menu $menu) use ($menuFactory) {
                    $menuFactory->subMenu($menu)
                        ->count(random_int(3, 5))
                        ->create();
                });
        }
    }
}
