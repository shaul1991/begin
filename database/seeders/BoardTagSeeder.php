<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Board;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class BoardTagSeeder extends Seeder
{
    public function run(): void
    {
        if (app()->environment() !== 'production') {
            Tag::each(function (Tag $tag) {
                $boards = Board::get()->random(random_int(0, 5));
                $tag->boards()->attach($boards);
            });
        }
    }
}
