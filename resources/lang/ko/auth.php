<?php

return [
    'failed' => '이메일 혹은 비밀번호가 일치하지 않습니다.',
    'password' => '잘못된 비밀번호 입니다.',
    'throttle' => ':seconds초 후 다시 시도하세요.',
];
