<?php

declare(strict_types=1);

namespace Tests\Http\Main;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MainTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function mainView(): void
    {
        $this->get(route('main'))
            ->assertOk()
            ->assertViewIs('main')
            ->assertSee(config('app.name'));
    }
}
