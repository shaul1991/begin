<?php

declare(strict_types=1);

namespace Tests\Http\Menu;

use App\Models\Menu;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MenuTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function menuView(): void
    {
        $menuFactory = Menu::factory();

        /** @var Menu $mainMenu */
        $mainMenu = $menuFactory->mainMenu()->create(['is_exposed' => true]);

        /** @var Menu $subMenu */
        $subMenu = $menuFactory->subMenu($mainMenu)->create(['is_exposed' => true]);

        $this->get(route('main'))
            ->assertOk()
            ->assertSee($mainMenu->name)
            ->assertSee($subMenu->name);
    }
}
