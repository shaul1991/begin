<?php

declare(strict_types=1);

namespace Tests\Http\Auth;

use App\Models\PointHistory;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function register()
    {
        $password = $this->faker->password;

        $args = [
            'name' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $this->post(route('register', $args))
            ->assertRedirect(route('main'));

        $this->assertAuthenticated();

        /** @var User $user */
        $user = auth()->user();

        $this->assertEquals(PointHistory::CAUSE_POINT[PointHistory::CAUSE_SIGN_UP], $user->point);
    }
}
