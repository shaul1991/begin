<?php

declare(strict_types=1);

namespace Tests\Http\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function forgot_password_page()
    {
        $this->assertGuest();

        $this->get(route('password.request'))
            ->assertOk()
            ->assertViewIs('auth.passwords.email');
    }

    /**
     * @test
     */
    public function already_authenticated_forgot_password_page()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('password.request'))
            ->assertRedirect();

        $this->assertAuthenticated();
    }
}
