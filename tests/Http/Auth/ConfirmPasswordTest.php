<?php

declare(strict_types=1);

namespace Tests\Http\Auth;


use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ConfirmPasswordTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function confirm_password_page()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('password.confirm'))
            ->assertOk()
            ->assertViewIs('auth.passwords.confirm');

        $this->assertAuthenticated();
    }

    /**
     * @test
     */
    public function not_authenticated_confirm_password_page()
    {
        $this->assertGuest();

        $this->get(route('password.confirm'))
            ->assertRedirect();
    }
}
