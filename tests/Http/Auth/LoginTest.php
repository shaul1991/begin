<?php

declare(strict_types=1);

namespace Tests\Http\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function login_page()
    {
        $this->assertGuest();

        $this->get(route('login'))
            ->assertOk()
            ->assertViewIs('auth.login');
    }

    /**
     * @test
     */
    public function login()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $args = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $this->assertGuest();

        $this->post('/login', $args)
            ->assertRedirect(route('main'));

        $this->assertAuthenticated();
    }

    /**
     * @test
     */
    public function already_authenticated()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('login'))
            ->assertRedirect(route('main'));

        $this->assertAuthenticated();
    }

    /**
     * @test
     */
    public function has_too_many_login_attempts_login()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $args = [
            'email' => $user->email,
            'password' => 'password1',
        ];

        $this->assertGuest();

        $this->post('/login', $args);
        $this->post('/login', $args);
        $this->post('/login', $args);
        $response = $this->post('/login', $args)
            ->assertStatus(Response::HTTP_FOUND);

        /** @var ValidationException $responseException */
        $responseException = $response->exception;

        $this->assertEquals(Response::HTTP_TOO_MANY_REQUESTS, $responseException->status);
    }
}
