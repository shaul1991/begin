<?php

declare(strict_types=1);

namespace Tests\Services\Menu;

use App\Models\Menu;
use App\Services\Menu\Dto\MainMenuDto;
use App\Services\Menu\Dto\SubMenuDto;
use App\Services\Menu\Exceptions\ExistsSubMenuException;
use App\Services\Menu\MenuService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MenuServiceTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function 메인메뉴를_생성한다(): void
    {
        /** @var Menu $menuData */
        $menuData = Menu::factory()->make();

        /** @var MenuService $menuService */
        $menuService = app(MenuService::class);

        $dto = new MainMenuDto(
            (string) $menuData->name,
            (bool) $menuData->is_exposed
        );

        /** @var Menu $mainMenu */
        $mainMenu = $menuService->mainMenuCreate($dto);

        $this->assertEquals((string) $menuData->name, $mainMenu->name);
        $this->assertEquals(Menu::DEPTH_MAIN, $mainMenu->depth);
        $this->assertEquals((bool) $menuData->is_exposed, $mainMenu->is_exposed);
    }

    /**
     * @test
     */
    public function 메인메뉴를_수정한다(): void
    {
        $menuFactory = Menu::factory();

        /** @var Menu $menuData */
        $menuData = $menuFactory->make();

        /** @var MenuService $menuService */
        $menuService = app(MenuService::class);

        /** @var Menu $mainMenu */
        $mainMenu = $menuFactory->mainMenu()->create();

        $dto = new MainMenuDto(
            (string) $menuData->name,
            (bool) $menuData->is_exposed
        );

        $result = $menuService->mainMenuUpdate($mainMenu, $dto);

        $this->assertTrue($result);
        $this->assertEquals((string) $menuData->name, $mainMenu->name);
        $this->assertEquals(Menu::DEPTH_MAIN, $mainMenu->depth);
        $this->assertEquals((bool) $menuData->is_exposed, $mainMenu->is_exposed);
    }

    /**
     * @test
     */
    public function 메인메뉴를_삭제한다(): void
    {
        $menuFactory = Menu::factory();

        /** @var MenuService $menuService */
        $menuService = app(MenuService::class);

        /** @var Menu $mainMenu */
        $mainMenu = $menuFactory->mainMenu()->create(['is_exposed' => true]);

        $menuService->mainMenuDestroy($mainMenu);

        $this->assertSoftDeleted($mainMenu);
    }

    /**
     * @test
     */
    public function 서브메뉴가_존재하여_메인메뉴를_삭제할_수_없다(): void
    {
        $this->expectException(ExistsSubMenuException::class);

        $menuFactory = Menu::factory();

        /** @var MenuService $menuService */
        $menuService = app(MenuService::class);

        /** @var Menu $mainMenu */
        $mainMenu = $menuFactory->mainMenu()->create(['is_exposed' => true]);

        $menuFactory->subMenu($mainMenu)->create();

        $menuService->mainMenuDestroy($mainMenu);
    }

    /**
     * @test
     */
    public function 서브메뉴를_생성한다(): void
    {
        $menuFactory = Menu::factory();
        /** @var Menu $menuData */
        $menuData = $menuFactory->make();

        /** @var MenuService $menuService */
        $menuService = app(MenuService::class);

        /** @var Menu $mainMenu */
        $mainMenu = $menuFactory->mainMenu()->create();

        $dto = new SubMenuDto(
            (string) $menuData->name,
            (bool) $menuData->is_exposed,
            (string) $menuData->url,
        );

        /** @var Menu $subMenu */
        $subMenu = $menuService->subMenuCreate($mainMenu, $dto);

        $this->assertEquals((string) $menuData->name, $subMenu->name);
        $this->assertEquals(Menu::DEPTH_SUB, $subMenu->depth);
        $this->assertEquals((bool) $menuData->is_exposed, $subMenu->is_exposed);
        $this->assertEquals((string) $menuData->url, $subMenu->url);
    }

    /**
     * @test
     */
    public function 서브메뉴를_수정한다(): void
    {
        $menuFactory = Menu::factory();

        /** @var Menu $menuData */
        $menuData = $menuFactory->make();

        /** @var MenuService $menuService */
        $menuService = app(MenuService::class);

        /** @var Menu $mainMenu */
        $mainMenu = $menuFactory->mainMenu()->create(['is_exposed' => true]);

        /** @var Menu $subMenu */
        $subMenu = $menuFactory->subMenu($mainMenu)->create(['is_exposed' => true]);

        $dto = new SubMenuDto(
            (string) $menuData->name,
            (bool) $menuData->is_exposed,
            (string) $menuData->url,
        );

        $menuService->subMenuUpdate($subMenu, $dto);

        $this->assertEquals((string) $menuData->name, $subMenu->name);
        $this->assertEquals(Menu::DEPTH_SUB, $subMenu->depth);
        $this->assertEquals((bool) $menuData->is_exposed, $subMenu->is_exposed);
        $this->assertEquals((string) $menuData->url, $subMenu->url);
    }
}
