<?php

declare(strict_types=1);

namespace Tests\Services\Point;

use App\Models\PointHistory;
use App\Models\User;
use App\Services\Point\Dto\SavePointDto;
use App\Services\Point\PointService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PointServiceTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     */
    public function 포인트를_더한다()
    {
        /** @var User $user */
        $user = User::factory()->create([
            'point' => 0,
        ]);

        $oldPoint = $user->point;

        /** @var PointService $pointService */
        $pointService = app(PointService::class);

        $cause = $this->faker->text;
        $point = $this->faker->randomNumber(3);

        $dto = new SavePointDto($user->id, $cause, $point);

        /** @var PointHistory $pointHistory */
        $pointHistory = $pointService->savePoint($dto);
        $pointService->syncPoint($user);

        $this->assertEquals($user->id, $pointHistory->user->id);
        $this->assertEquals($user->point, ($oldPoint + $pointHistory->point));
        $this->assertEquals($cause, $pointHistory->cause);
        $this->assertEquals($point, $pointHistory->point);
    }

    /**
     * @test
     */
    public function 포인트를_뺀다()
    {
        /** @var User $user */
        $user = User::factory()->create([
            'point' => 0,
        ]);

        /** @var PointService $pointService */
        $pointService = app(PointService::class);

        $cause = $this->faker->text;
        $point = $this->faker->randomNumber(3);

        $dto = new SavePointDto($user->id, $cause, $point);
        $pointService->savePoint($dto);

        /** @var PointHistory $pointHistory */
        $dto = new SavePointDto($user->id, 'sub_'.$cause, (int) -$point);
        $pointHistory = $pointService->savePoint($dto);

        $pointService->syncPoint($user);

        $this->assertEquals($user->id, $pointHistory->user->id);
        $this->assertEquals(0, $user->point);
        $this->assertEquals('sub_'.$cause, $pointHistory->cause);
        $this->assertEquals(-$point, $pointHistory->point);
    }
}
