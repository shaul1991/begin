<?php

declare(strict_types=1);

namespace Tests\Services\Board;

use App\Models\Board;
use App\Models\User;
use App\Services\Board\BoardService;
use App\Services\Board\Dto\BoardDto;
use App\Services\Board\Exceptions\NotMatchedBoardWriterException;
use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class BoardServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @throws Exception
     */
    public function 모든_게시글을_조회한다()
    {
        $randomCount = random_int(0, 10);

        Board::factory()->count($randomCount)->create();

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $this->assertCount($randomCount, $boardService->all());
    }

    /**
     * @test
     * @throws Exception
     */
    public function 모든_공개_게시글을_조회한다()
    {
        $publicRandomCount = random_int(0, 10);
        $privateRandomCount = random_int(0, 10);

        Board::factory()->count($publicRandomCount)->create(['is_public' => true]);
        Board::factory()->count($privateRandomCount)->create(['is_public' => false]);

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $this->assertCount($publicRandomCount, $boardService->getPublicBoards());
    }

    /**
     * @test
     * @throws Exception
     */
    public function 모든_비공개_게시글을_조회한다()
    {
        $publicRandomCount = random_int(0, 10);
        $privateRandomCount = random_int(0, 10);

        Board::factory()->count($publicRandomCount)->create(['is_public' => true]);
        Board::factory()->count($privateRandomCount)->create(['is_public' => false]);

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $this->assertCount($privateRandomCount, $boardService->getPrivateBoards());
    }

    /**
     * @test
     * @throws Exception
     */
    public function 특정유저의_모든_게시글을_조회한다()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $randomCount = random_int(0, 10);

        Board::factory()->count($randomCount)->create(['user_id' => $user->id]);

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $this->assertCount($randomCount, $boardService->all($user->id));
    }


    /**
     * @test
     * @throws Exception
     */
    public function 특정_유저의_공개_게시글을_조회한다()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $publicRandomCount = random_int(0, 10);
        $privateRandomCount = random_int(0, 10);

        Board::factory()->count($publicRandomCount)->create([
            'user_id' => $user->id,
            'is_public' => true,
        ]);
        Board::factory()->count($privateRandomCount)->create([
            'user_id' => $user->id,
            'is_public' => false,
        ]);

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $this->assertCount($publicRandomCount, $boardService->getPublicBoards($user->id));
    }

    /**
     * @test
     * @throws Exception
     */
    public function 특정_유저의_비공개_게시글을_조회한다()
    {
        /** @var User $user */
        $user = User::factory()->create();
        $publicRandomCount = random_int(0, 10);
        $privateRandomCount = random_int(0, 10);

        Board::factory()->count($publicRandomCount)->create([
            'user_id' => $user->id,
            'is_public' => true,
        ]);

        Board::factory()->count($privateRandomCount)->create([
            'user_id' => $user->id,
            'is_public' => false,
        ]);

        Board::factory()->count($publicRandomCount + 1)->create([
            'is_public' => true,
        ]);

        Board::factory()->count($privateRandomCount + 1)->create([
            'is_public' => false,
        ]);

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $this->assertCount($privateRandomCount, $boardService->getPrivateBoards($user->id));
    }

    /**
     * @test
     */
    public function 글을_작성할_수_있다()
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Board $boardData */
        $boardData = Board::factory()->make();

        $dto = new BoardDto(
            (string) $boardData->title,
            (string) $boardData->contents,
            (bool) $boardData->is_public
        );

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        /** @var Board $board */
        $board = $boardService->save($user, $dto);

        $this->assertDatabaseHas($board->getTable(), $dto->toArray());

        $this->assertEquals($board->user_id, $user->id);
        $this->assertEquals($boardData->title, $board->title);
        $this->assertEquals($boardData->contents, $board->contents);
        $this->assertEquals($boardData->is_public, $board->is_public);
        $this->assertEquals(0, $board->views);
    }

    /**
     * @test
     */
    public function 자신의_글을_수정할_수_있다()
    {
        $boardFactory = Board::factory();

        /** @var Board $board */
        $board = $boardFactory->create();

        $user = $board->user;

        /** @var Board $boardUpdateData */
        $boardUpdateData = $boardFactory->make();

        $dto = new BoardDto(
            (string) $boardUpdateData->title,
            (string) $boardUpdateData->contents,
            (bool) $boardUpdateData->is_public
        );

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $updateBoard = $boardService->update($user, $board, $dto);

        $this->assertEquals($board->id, $updateBoard->id);
        $this->assertEquals($boardUpdateData->title, $updateBoard->title);
        $this->assertEquals($boardUpdateData->contents, $updateBoard->contents);
        $this->assertEquals($boardUpdateData->is_public, $updateBoard->is_public);
        $this->assertEquals($user->id, $board->user_id);
        $this->assertEquals($user->id, $updateBoard->user_id);
    }

    /**
     * @test
     */
    public function 자신의_글을_자신외에_수정할_수_없다()
    {
        $this->expectException(NotMatchedBoardWriterException::class);

        /** @var User $user */
        $user = User::factory()->create();

        $boardFactory = Board::factory();

        /** @var Board $board */
        $board = $boardFactory->create();

        /** @var Board $boardUpdateData */
        $boardUpdateData = $boardFactory->make();

        $dto = new BoardDto(
            (string) $boardUpdateData->title,
            (string) $boardUpdateData->contents,
            (bool) $boardUpdateData->is_public
        );

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $boardService->update($user, $board, $dto);
    }

    /**
     * @test
     */
    public function 자신의_글을_삭제할_수_있다()
    {
        $boardFactory = Board::factory();

        /** @var Board $board */
        $board = $boardFactory->create();

        $user = $board->user;

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $boardService->destroy($user, $board);

        $this->assertSoftDeleted($board);
    }

    /**
     * @test
     */
    public function 자신의_글을_자신외에_삭제할_수_없다()
    {
        $this->expectException(NotMatchedBoardWriterException::class);

        $boardFactory = Board::factory();

        /** @var Board $board */
        $board = $boardFactory->create();

        /** @var User $user */
        $user = User::factory()->create();

        /** @var BoardService $boardService */
        $boardService = app(BoardService::class);

        $boardService->destroy($user, $board);

        $this->assertSoftDeleted($board);
    }
}
