<?php

declare(strict_types=1);

namespace Tests\Services\User;

use App\Models\User;
use App\Services\User\UpdateLastLoginService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateLastLoginTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * @test
     * @throws AuthenticationException
     */
    public function 로그인_시_최근_로그인_정보를_수정한다()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this->actingAs($user);

        /** @var UpdateLastLoginService $updateLastLoginService */
        $updateLastLoginService = app(UpdateLastLoginService::class);

        $requestIp = $this->faker->ipv4;

        $user = $updateLastLoginService->update($user, $requestIp);

        $this->assertAuthenticated();
        $this->assertEquals($requestIp, $user->last_login_ip);
    }
}
