<?php

declare(strict_types=1);

namespace Tests\Services\User;

use App\Models\User;
use App\Services\User\Exceptions\CannotChangeLevelYourselfException;
use App\Services\User\Exceptions\CannotGrantSameOrHigherAuthorityThanYourselfException;
use App\Services\User\Exceptions\ManagerOrHigherPrivilegeIsRequiredException;
use App\Services\User\UpdateUserLevelService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UpdateUserLevelTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @throws CannotChangeLevelYourselfException
     * @throws CannotGrantSameOrHigherAuthorityThanYourselfException
     * @throws ManagerOrHigherPrivilegeIsRequiredException
     */
    public function 등급을_정상적으로_수정할_수_있다()
    {
        /** @var User $admin */
        $admin = User::factory()->create([
            'level' => User::USER_LEVEL_ADMIN,
        ]);

        /** @var User $user */
        $user = User::factory()->create([
            'level' => User::USER_LEVEL_GENERAL,
        ]);

        /** @var UpdateUserLevelService $updateUserLevelService */
        $updateUserLevelService = app(UpdateUserLevelService::class);

        $updateUserLevelService->update($admin, $user, User::USER_LEVEL_MANAGER);

        $this->assertEquals(User::USER_LEVEL_MANAGER, $user->level);
    }

    /**
     * @test
     * @throws CannotChangeLevelYourselfException
     * @throws CannotGrantSameOrHigherAuthorityThanYourselfException
     * @throws ManagerOrHigherPrivilegeIsRequiredException
     */
    public function 등급을_조정을_위해서는_매니저_등급이하는_조정할_수_없다()
    {
        $this->expectException(ManagerOrHigherPrivilegeIsRequiredException::class);

        /** @var User $admin */
        $admin = User::factory()->create([
            'level' => User::USER_LEVEL_GENERAL,
        ]);

        /** @var User $user */
        $user = User::factory()->create([
            'level' => User::USER_LEVEL_WAITING,
        ]);

        /** @var UpdateUserLevelService $updateUserLevelService */
        $updateUserLevelService = app(UpdateUserLevelService::class);

        $updateUserLevelService->update($admin, $user, User::USER_LEVEL_GENERAL);
    }

    /**
     * @test
     * @throws CannotChangeLevelYourselfException
     * @throws CannotGrantSameOrHigherAuthorityThanYourselfException
     * @throws ManagerOrHigherPrivilegeIsRequiredException
     */
    public function 자기_자신의_등급을_조정할_수_없다()
    {
        $this->expectException(CannotChangeLevelYourselfException::class);

        /** @var User $user */
        $user = User::factory()->create([
            'level' => User::USER_LEVEL_MANAGER,
        ]);

        /** @var UpdateUserLevelService $updateUserLevelService */
        $updateUserLevelService = app(UpdateUserLevelService::class);

        $updateUserLevelService->update($user, $user, User::USER_LEVEL_ADMIN);
    }

    /**
     * @test
     * @throws CannotChangeLevelYourselfException
     * @throws CannotGrantSameOrHigherAuthorityThanYourselfException
     * @throws ManagerOrHigherPrivilegeIsRequiredException
     */
    public function 자신보다_높은_등급_혹은_같은_등급으로_조정할_수_없다()
    {
        $this->expectException(CannotGrantSameOrHigherAuthorityThanYourselfException::class);

        /** @var User $admin */
        $admin = User::factory()->create([
            'level' => User::USER_LEVEL_ADMIN,
        ]);

        /** @var User $user */
        $user = User::factory()->create([
            'level' => User::USER_LEVEL_MANAGER,
        ]);

        /** @var UpdateUserLevelService $updateUserLevelService */
        $updateUserLevelService = app(UpdateUserLevelService::class);

        $updateUserLevelService->update($admin, $user, User::USER_LEVEL_ADMIN);
    }
}
