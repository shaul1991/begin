<?php

use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', MainController::class)
    ->name('main');
